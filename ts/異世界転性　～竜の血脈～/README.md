# novel

- title: 異世界転性　～竜の血脈～
- title_zh1: 異世界轉性龍之血脈
- author: 彦猫
- illust:
- source: http://ncode.syosetu.com/n7709cn/
- cover:
- publisher: syosetu
- date: 2018-04-25T09:25:00+08:00
- status: 完結済
- novel_status: 0x0001

## illusts


## publishers

- syosetu

## series

- name: 竜の血脈シリーズ

## preface


```
　剣と武術に明け暮れて己を鍛えた武術家（４２歳独身）は、大規模な災害に巻き込まれ、多くの命を救ってその生涯を終えた。このままあの世行きか、と思った男の前に現れたのは、管理官を名乗る美女。剣と魔法の世界に転生できると聞いて、男は強力なギフト『竜の血脈』を持ち転生する。しかしその転生した先は女だった。しかも美少女だった。武の道を究めるため、男の象徴を取り戻すため、生まれ変わった少女？の冒険？が始まる。
　とりあえず美人のママのおっぱいには顔を埋めますけどね。女風呂では他のおっぱいガン見しますけどね！ 可愛い女の子にはセクハラかましますけどね！
　※十五少年少女異世界冒険記は続編のような外伝にあたります。よろしければそちらもどうぞ。
　※勇者を殺せ！？　～神竜の騎士～　は完全に続編にあたります。
```

## tags

- node-novel
- R15
- TS
- syosetu
- ガールズラブ
- チート
- ドラゴン
- ハイファンタジー
- ハイファンタジー〔ファンタジー〕
- ハーレム？
- ファンタジー
- モテモテ？
- 冒険
- 刀主人公
- 完結済
- 戦記
- 残酷な描写あり
- 異世界
- 異世界転生
- 魔王

# contribute

- 迪斯托爾夏夫特
- ks102

# options

## downloadOptions

- noFilePadend: true
- filePrefixMode: 1
- startIndex: 1

## syosetu

- txtdownload_id:
- series_id: s9654c
- novel_id: n7709cn

## textlayout

- allow_lf2: false

# link

- [narou.nar.jp](https://narou.nar.jp/search.php?text=n7709cn&novel=all&genre=all&new_genre=all&length=0&down=0&up=100) - 小説家になろう　更新情報検索
- [小説情報](https://ncode.syosetu.com/novelview/infotop/ncode/n7709cn/)
- [竜の血脈シリーズ](http://ncode.syosetu.com/s9654c/)
- http://pan.baidu.com/s/1XQ-3qxXUarFx02-p7pje7w
- [异世界转性龙之血脉吧](https://tieba.baidu.com/f?kw=%E5%BC%82%E4%B8%96%E7%95%8C%E8%BD%AC%E6%80%A7%E9%BE%99%E4%B9%8B%E8%A1%80%E8%84%89&ie=utf-8 "异世界转性龙之血脉")
- 

