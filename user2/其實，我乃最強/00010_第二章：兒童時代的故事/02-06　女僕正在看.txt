為萊斯所準備的房間是城堡中最高級的貴賓室。
趴在帶有篷子的床上，把拳頭打在枕頭上。

「可惡，可惡！為什麼？為什麼我會輸給那樣最底層的垃圾呢！」

難以置信。不能承認。在現實生活中，不可能遇到。

（我是王子，是繼承閃光公主的血統的下一任國王！）

被魔力總量只有二十分之一的對手，非但沒有擊倒對手，反而被單方面的打敗，這是騙人的。

「不是！那傢伙竭盡全力躲避我的攻擊。所以，不能說是單方面的……」

不願去想，也想相信實際上是不同的。
但是，回想起來，背後卻充滿了恐懼。

最後，哈特做了什麼？
確實發動了的魔法，沒有任何痕跡就消失了。魔法無效化的魔法。這是只有曾經的【大賢者】才能做到的絕招。

「為什麼會這樣……」

視察之類的麻煩事，根本沒有參加的意思。

我遠道而來，是因為母親的命令。
為了找出國王派中首屈一指的芬菲斯卿的【弱點】。要是能讓他下臺就好了。或是發言力被削弱的程度也足夠了。
萊斯所賦予的任務就是這個。

我覺得很麻煩。儘管是小孩，還是很不滿，認為這不是王子該做的工作。
但是告訴自己，這是被母親認可的絕好機會，於是千里迢迢地趕來了。

如果失敗了，媽媽會怎麼做呢？

——如果是你的話，應該可以吧？

想起那冰冷的笑容，難以言喻的恐懼遍佈全身。
萊斯搖著頭左右搖晃，像是擺脫了令人不快的想像一樣。

為了完成任務，在路上想了很多。

他想，比起發現芬菲斯卿的【弱點】，不如刻意做出【弱點】比較好。因此，如果自己採取傲慢的態度，將他兒子狠狠地揍一頓的話，肯定會對王子不敬的。

雖然是孩子的淺薄智慧，但對9歲的他來說已經是極限了。
就算芬菲斯卿的行動不如預期，也應該會對王子的任性保持警戒。趁著他把注意力轉向這邊的時候，讓護衛騎士們在背後活動。因為他們也接受了王妃的密令。


萊斯自己，『地』的性格能推進的好的作戰。不用擺出一副好孩子架子的演技。這樣想著……。

「王子殿下，失禮了。」

門被敲了，沒等回答，幾名騎士冒冒失失地進來了。

「差不多到晚餐會的時間了。請先做好準備。」

接著侍女們出現，請他換衣裝。
為了歡迎視察團的晚宴，說實話，我不想見到哈特。雖然是小孩子，不對，正因為是小孩，本能才會低聲私語。

——那是怪物。

其中一個騎士在他換衣服的時候，還沒有離開房間。

「明天的農地視察，請邀請芬菲斯的女兒。」

「啊？為什麼突然這麼說。」

「在路上需要說話的對象吧。如果那樣的話，年齡相近的夏洛特小姐不是比較適合嗎？」他這樣說，我就說不下去了。

對於帶有嘲笑的說法，咬緊牙關也是可以。

「……是母親的指示嗎？」

「請理解為考慮王子殿下的事。」

因為硬要隱瞞主語，所以理解了。

（但是，母親在想什麼呢……？對方是七歲的小孩吧？能得到和邊境伯弱點相關的情報嗎？）

但是，連母親的想法都沒有想到——。




即使萊斯前往晚餐會，騎士們也在他的房間裡密談。
給王子用的貴賓室都設有多層的防護結界，不會漏音。我利用了那個。

「真是的，王子多管閒事。而且在決勝負的時候居然還亂殺。回到王宮的話，會被王妃大人狠狠地罵吧。」

最年長的騎士說的話，失笑起來了。

「不管怎麼說，計畫不受影響。正式演出是明天。在這個森林的地方，王子和公主，還有襲擊目標乘坐的馬車。」

年長的騎士指向桌子展開了的地圖。

「我和幾名部下一起帶著三名部下逃走，將他們帶到這個地點。」

年輕的騎士「在那裡使用襲擊的巨大召喚獸」。

「是的。我們會好好的，讓召喚獸殺死目標。讓公主被冤枉就可以了，但最優先事項是目標要確實抹殺。你們在馬車附近阻止芬菲斯卿的腳步。」

「為了讓您全力以赴，我們會阻止他的。明白了。」

目標地點已經部署了工作部隊。進行襲擊的是偽裝成魔物的召喚獸，沒有人可以阻止。
如果王子或公主都受傷的話，也可以譴責芬菲斯卿疏於確保安全。

「別拖延啊？如果失敗了，小心會被光之箭貫穿。」

閃光公主毫不留情。如此大的作戰，不允許失敗。

騎士們咽了口水，離開了房間。

那個，從頭到尾——。





「哼，卑鄙的傢伙們。」

女僕看到了！

芙蕾在打掃走廊時，用哈特寄放的遠端監視用結界，窺視著萊斯的房間。
他們的陰謀，毫無保留地——。